<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class SaladFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => '',
            'description' => $this->faker->words(7, true),
            'carbohydrates' => 0,
            'protein' => 0,
            'fat' => 0,
            'calories' => 0,
            'sugar' => 0,
            'weight' => 0,
        ];
    }
}
