<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fruit_salad', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('weight');
            $table->foreignId('salad_id')->constrained('salads')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('fruit_id')->constrained('fruits')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fruit_salad');
    }
};
