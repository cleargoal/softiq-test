<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salads', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            $table->unsignedDecimal('carbohydrates', 4, 1)->index();
            $table->unsignedDecimal('protein', 4, 1)->index();
            $table->unsignedDecimal('fat', 4, 1)->index();
            $table->unsignedSmallInteger('calories')->index();
            $table->unsignedDecimal('sugar', 4, 1)->index();
            $table->unsignedSmallInteger('weight')->index()->comment('in grams');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salads');
    }
};
