<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fruits', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedDecimal('carbohydrates', 4, 1)->index();
            $table->unsignedDecimal('protein', 4, 1)->index();
            $table->unsignedDecimal('fat', 4, 1)->index();
            $table->unsignedSmallInteger('calories')->index();
            $table->unsignedDecimal('sugar', 4, 1)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fruits');
    }
};
