<?php
return [
    'Wheat',
    'Oats',
    'Barley',
    'Rye',
    'Sorghum',
    'Corn',
    'Quinoa',
    'Buckwheat',
    'Millet',
    'Triticale',
    'Grape Nuts',
    'Granola',
];
