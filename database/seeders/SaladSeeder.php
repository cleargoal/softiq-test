<?php

namespace Database\Seeders;

use App\Models\Fruit;
use App\Models\Salad;
use App\Services\SaladService;
use Illuminate\Database\Seeder;

class SaladSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seeding();
        $this->makeRelations();
        (new SaladService())->putSaladsListToRedis();
    }

    /**
     * Seeding
     *
     * @readonly void
     */
    protected function seeding()
    {
        $randomNames =
            collect(['Summer', 'Asian', 'Slaw', 'Best', 'Broccoli', 'Shredded', 'Brussels', 'Sprout', 'Easy', 'Pasta', 'Rainbow', 'Orzo', 'Salad', 'Creamy', 'Vegan',
                'Cherry', 'Tomato', 'Couscous', 'Sesame', 'Soba', 'Noodles', 'Healthy', 'Taco', 'Cobb', 'With', 'Coconut', 'Bacon', 'Grilled', 'Romaine', 'Caesar', 'Wedges',]);

        for ($i = 0; $i < 500; $i++) {
            $item = $randomNames->random(rand(3, 4))->implode(' ');
                Salad::factory()
                ->create(['name' => $item]);
        }
    }

    /**
     * Make relations salad to fruits
     *
     */
    protected function makeRelations()
    {
        $service = new SaladService();
        $fruits = Fruit::all();

        Salad::chunk(100, function ($salads) use($service, $fruits) {

            foreach ($salads as $salad) {
                $fruitIds = $fruits->random(rand(2, 5))->pluck('id')->toArray();

                foreach ($fruitIds as $fruitId) {
                    $salad->fruits()->attach($fruitId, ['weight' => rand(72, 190)]);
                }
                $saladN = Salad::where('id', $salad->id)->with('fruits')->first();
                $saladN->fruits->each(function ($item) {
                    $item->weight = $item->ingredient->weight;
                }
                );

                $saladData = $service->prepareModelData($saladN->toArray());

                $salad->update($saladData);
            }
        });

    }

}
