<?php

namespace Database\Seeders;

use App\Models\Fruit;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class FruitSeeder extends Seeder
{
    private Collection $fruits;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->getDataFromFile();
        $this->callFactory();
    }

    /**
     * Get data from file
     *
     * @return void
     */
    protected function getDataFromFile()
    {
        $isFile = 'database/migrations/src/fruits.json';
        if(!file_exists($isFile)) {
            $this->command->error('No source file');
        }
        $fileContent = file_get_contents($isFile);
        $this->fruits = collect(json_decode($fileContent));
    }

    /**
     * Send data to Factory
     *
     */
    protected function callFactory()
    {
        foreach ($this->fruits as $fruit) {
            Fruit::factory(1)
                ->create([
                    'name' => $fruit->name,
                    'carbohydrates' => $fruit->nutrients->carbohydrates,
                    'protein' => $fruit->nutrients->protein,
                    'fat' => $fruit->nutrients->fat,
                    'calories' => $fruit->nutrients->calories,
                    'sugar' => $fruit->nutrients->sugar,
                ]);
        }
    }
}
