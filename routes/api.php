<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users', '\App\Http\Controllers\UserController@index');
Route::post('/login', '\App\Http\Controllers\AuthController@login')->name('login');

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/current-user', '\App\Http\Controllers\AuthController@currentUser')->name('current.user');
    Route::get('/logout', '\App\Http\Controllers\AuthController@logout')->name('logout');
    Route::get('/salads-list', '\App\Http\Controllers\SaladController@recipesList')->name('salads.list');
    Route::apiResource('/salads', \App\Http\Controllers\SaladController::class);
    Route::apiResource('/fruits', \App\Http\Controllers\FruitController::class);
    Route::post('salads/search/fruits', '\App\Http\Controllers\SaladController@searchSaladsByFruits')->name('salad.search.with.fruits');
    Route::post('salads/search/nutrients', '\App\Http\Controllers\SaladController@searchSaladsByNutrients')->name('salad.search.with.nutrients');
    Route::post('salads/search/fruits/weight', '\App\Http\Controllers\SaladController@searchSaladsFruitsWeight')->name('salad.search.fruits.weight');
    Route::post('salads/sort', '\App\Http\Controllers\SaladController@sortSaladsByNutrients')->name('salad.sort.with.nutrients');
});

