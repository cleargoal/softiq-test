## run project on Linux
# Docker Containers
### Project uses the Laravel's default containers set named 'Sail'.

# Installation:
### 1. clone project from gitlab
<code>git clone git@gitlab.com:cleargoal/softiq-test.git</code>

### 2. copy environment setup
enter to project folder;
<p>through your file manager copy file 
<code> .env.example </code> to <code> .env </code></p>
<p>or run command in console:</p>
<code>cp .env.example .env</code>

### 3. build and start containers
<p>in console run next (copy the whole block to clipboard):</p>
docker run --rm \<br>
    -u "$(id -u):$(id -g)" \<br>
    -v $(pwd):/var/www/html \<br>
    -w /var/www/html \<br>
    laravelsail/php81-composer:latest \<br>
    composer install --ignore-platform-reqs

#### 4. run containers (next time when they built but stopped)
<code> ./vendor/bin/sail up -d </code>

### 5. create tables and seed them with dummy data
<code> ./vendor/bin/sail artisan migrate --seed </code>

now you can use Postman or analog to make requests, Postman collection included.

### 6. If for any reason you want to refresh DB to starting data set, run this command
<code> ./vendor/bin/sail artisan migrate:refresh --seed </code>

## Authentication:
1) Use endpoint 'Users list' to choose any randomly created user. The password of all of them is 'password'.
2) Then use endpoint 'Login' and enter to it chosen user's email.
3) The Postman collection contains 'token' variable for Bearer token that will be used automatically.

## Searching
### Search by Fruits IDs
This search is to find salads with at least 1 fruit mentioned in request.

The FormRequest receives the array of IDs of salad components (fruits) and returns the salad list where every salad contains at least 1 fruit mentioned in the request.

### Search by nutrients
The FormRequest receives nutrients name(s) as keys of array of nutrients values.

The values array may contain 1 or 2 values.

If data array contains 1 value - search is exactly, 
if data array contains 2 values - searches between them.

### Search by ingredient (fruits) weight in the salad
It's try to use data from the pivot table.

The request parameter 'key' is the fruit ID.

The request parameter 'range' is the range of weight of fruit in the salad.

## Sorting
The sorting performs on one or more salad nutrients.

#### Sort keys: 
<ul>
<li>"calories": "desc",</li> 
<li>"fat": "desc", </li> 
<li>"sugar": "asc", </li>
<li>"protein": "desc",</li> 
<li>"carbohydrates": "asc",</li>
<li>"weight": "desc"</li>
</ul>
Off course, you can use as 'asc', as 'desc' sorting directions for any sorting key.

It's possible to use one or more keys. 

When you change the order of these keys in the request, sorting changes respectively.
