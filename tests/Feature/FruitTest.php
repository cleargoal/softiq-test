<?php

namespace Tests\Feature;

use App\Models\Fruit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FruitTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_name()
    {
        $fruit = new Fruit();
        $fruit->name = 'Lemon';
        $this->assertIsString($fruit->name);
        $this->assertStringMatchesFormat(ucfirst('Lemon'), $fruit->name);
    }
}
