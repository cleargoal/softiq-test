<?php

namespace Tests\Feature;

use App\Http\Controllers\FruitController;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FruitControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_show()
    {
        $controller = new FruitController();
        $fruitExist = $controller->show(1);
//        dd($fruitExist);
        $this->assertTrue((bool)$fruitExist);
        $this->assertIsString($fruitExist->content());
        $fruitNull = $controller->show(32);
        $this->assertStringContainsString('Fruit with id 32 not found', $fruitNull);
    }
}
