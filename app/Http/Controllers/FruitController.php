<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFruitRequest;
use App\Http\Requests\UpdateFruitRequest;
use App\Models\Fruit;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class FruitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Fruit::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFruitRequest $request
     * @return JsonResponse
     */
    public function store(StoreFruitRequest $request)
    {
        return response()->json(Fruit::create($request->all()) ?? "Cannot create fruit $request->name");
    }

    /**
     * Display the specified resource.
     *
     * @param $fruit
     * @return JsonResponse
     */
    public function show($fruit): JsonResponse
    {
        $getFruit = Fruit::find($fruit);
        return response()->json($getFruit ?? "Fruit with id $fruit not found");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFruitRequest  $request
     * @param Fruit $fruit
     * @return Response
     */
    public function update(UpdateFruitRequest $request, Fruit $fruit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Fruit $fruit
     * @return JsonResponse
     */
    public function destroy(Fruit $fruit): JsonResponse
    {
        return response()->json($fruit->delete() ? 'Deleted successfully.' : 'Fruit not deleted');
    }
}
