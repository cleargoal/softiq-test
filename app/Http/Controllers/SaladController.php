<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaladSearchFruitsRequest;
use App\Http\Requests\SaladSearchNutrientsRequest;
use App\Http\Requests\SaladSearchWeightRequest;
use App\Http\Requests\StoreSaladRequest;
use App\Http\Requests\UpdateSaladRequest;
use App\Http\Resources\SaladListResource;
use App\Http\Resources\SaladResource;
use App\Http\Resources\SaladSearchFruitsWeightResource;
use App\Http\Resources\SaladSortResource;
use App\Models\Salad;
use App\Services\SaladService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

class SaladController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Salad::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSaladRequest $request
     * @param SaladService $saladService
     *
     * @return JsonResponse
     */
    public function store(StoreSaladRequest $request, SaladService $saladService): JsonResponse
    {
        $salad = $saladService->createSalad($request->all());
        return response()->json(new SaladResource($salad) ?? "Cannot create salad $request->name");
    }

    /**
     * Display the specified resource.
     *
     * @param $saladId
     * @param SaladService $service
     * @return JsonResponse
     */
    public function show($saladId, SaladService $service): JsonResponse
    {
        $showSalad = $service->getSaladFromRedis($saladId);
        return response()->json($showSalad !== false ? (new SaladResource($showSalad)) : "The salad with ID $saladId not found");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSaladRequest $request
     * @param Salad $salad
     * @param SaladService $saladService
     *
     * @return JsonResponse
     */
    public function update(UpdateSaladRequest $request, Salad $salad, SaladService $saladService): JsonResponse
    {
        $updated = $saladService->updateSalad($request->all(), $salad);
        return response()->json(new SaladResource($updated) ?? "Cannot update salad $salad->name");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $salad
     * @param SaladService $saladService
     * @return JsonResponse
     */
    public function destroy(int $salad, SaladService $saladService): JsonResponse
    {
        return response()->json($saladService->removeSalad($salad) === true ? 'Salad deleted successfully!' : "Cannot delete salad, it's not exist");
    }

    /**
     * List of all recipes with fields: recipe names, names of fruits used, total weight and total calories
     *
     * @param SaladService $service
     * @return JsonResponse
     */
    public function recipesList(SaladService $service): JsonResponse
    {
        $saladsList = $service->getSaladsListFromRedis();
        return response()->json(SaladListResource::collection($saladsList));
    }

    /**
     * Search salad(s) by containing fruit(s)
     *
     * @param SaladSearchFruitsRequest $request
     * @param SaladService $service
     * @return JsonResponse
     */
    public function searchSaladsByFruits(SaladSearchFruitsRequest $request, SaladService $service): JsonResponse
    {
        $foundSalads = $service->searchSaladsFruits($request->all());
        return response()->json(SaladListResource::collection($foundSalads));
    }

    /**
     * Search salad(s) by containing nutrient(s)
     *
     * @param SaladSearchNutrientsRequest $request
     * @param SaladService $service
     * @return JsonResponse
     */
    public function searchSaladsByNutrients(SaladSearchNutrientsRequest $request, SaladService $service): JsonResponse
    {
        $foundSalads = $service->searchSaladsNutrients($request->all());
        return response()->json(SaladSortResource::collection($foundSalads));
    }

    /**
     * Search salad(s) by nutrient(s) weight
     *
     * @param SaladSearchWeightRequest $request
     * @param SaladService $service
     * @return JsonResponse
     */
    public function searchSaladsFruitsWeight(SaladSearchWeightRequest $request, SaladService $service): JsonResponse
    {
        $foundSalads = $service->searchSaladsFruitsWeight($request->all());
        return response()->json(SaladSearchFruitsWeightResource::collection($foundSalads));
    }

    /**
     * sort Salads By Nutrients
     *
     * @param Request $request
     * @param SaladService $service
     * @return JsonResponse
     */
    public function sortSaladsByNutrients(Request $request, SaladService $service): JsonResponse
    {
        $foundSalads = $service->sortSaladsByNutrients($request->all());
        return response()->json(SaladSortResource::collection($foundSalads));
    }

    /**
     * sort Salads By Nutrients
     *
     * @param Request $request
     * @param SaladService $service
     * @return JsonResponse
     */
    public function filterSaladsByNutrients(Request $request, SaladService $service): JsonResponse
    {
        $foundSalads = $service->sortSaladsByNutrients($request->all());
        return response()->json(SaladSortResource::collection($foundSalads));
    }

}
