<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFruitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3',
            'carbohydrates' => 'required|numeric|gte:0',
            'protein' => 'required|numeric|gte:0',
            'fat' => 'required|numeric|gte:0',
            'calories' => 'required|integer|numeric|gte:0',
            'sugar' => 'required|numeric|gte:0',
        ];
    }
}
