<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSaladRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:salads|min:3',
            'description' => 'required|string|min:20',
            'fruits' => 'required|array|min:2',
            'fruits.*.id' => 'required|integer|min:1',
            'fruits.*.weight' => 'required|numeric|min:0',
        ];
    }

}
