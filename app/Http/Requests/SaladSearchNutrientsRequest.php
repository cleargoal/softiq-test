<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaladSearchNutrientsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fat' => 'array|min:1|max:2',
            'protein' => 'array|min:1|max:2',
            'calories' => 'array|min:1|max:2',
            'sugar' => 'array|min:1|max:2',
            'weight' => 'array|min:1|max:2',
            'carbohydrates' => 'array|min:1|max:2',
        ];
    }
}
