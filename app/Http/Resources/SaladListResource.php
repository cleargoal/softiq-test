<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SaladListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'weight' => $this->weight,
            'calories' => $this->calories,
            'fruits' => FruitListResource::collection($this->fruits)->implode('name', ', '),
        ];
    }
}
