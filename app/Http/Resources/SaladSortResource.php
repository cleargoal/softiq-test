<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SaladSortResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'weight' => $this->weight,
            'calories' => $this->calories,
            'fat' => $this->fat,
            'sugar' => $this->sugar,
            'protein' => $this->protein,
            'carbohydrates' => $this->carbohydrates,
            'fruits' => FruitListResource::collection($this->fruits)->implode('name', ', '),
        ];
    }
}
