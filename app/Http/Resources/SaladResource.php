<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SaladResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'carbohydrates' => $this->carbohydrates,
            'protein' => $this->protein,
            'fat' => $this->fat,
            'calories' => $this->calories,
            'sugar' => $this->sugar,
            'weight' => $this->weight,
            'fruits' => $this->fruits,
        ];
    }
}
