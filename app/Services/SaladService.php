<?php

namespace App\Services;

use App\Models\Fruit;
use App\Models\Salad;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redis;

class SaladService
{

    protected Collection $inputFruits;

    /**
     * Create salad with fruits
     *
     * @param
     * @return mixed
     */
    public function createSalad($inputData): mixed
    {
        $salad = $this->prepareModelData($inputData);
        $newSalad = Salad::create($salad);

        foreach ($inputData['fruits'] as $ingredient) {
            $newSalad->fruits()->attach($ingredient['id'], ['weight' => $ingredient['weight']]);
        }

        $this->putSaladToRedis($newSalad->id);

        return $newSalad;
    }

    /**
     * Update salad
     *
     * @param $inputData
     * @param $updateSalad
     *
     * @return mixed
     */
    public function updateSalad($inputData, $updateSalad): mixed
    {
        $saladData = $this->prepareModelData($inputData);

        $updateSalad->update($saladData);
        $updateSalad->fruits()->detach();

        foreach ($inputData['fruits'] as $ingredient) {
            $updateSalad->fruits()->attach($ingredient['id'], ['weight' => $ingredient['weight']]);
        }

        $this->putSaladToRedis($updateSalad->id);

        return $updateSalad;
    }

    /**
     * Prepare model data
     *
     * @param $inputData
     * @return array
     */
    public function prepareModelData($inputData): array
    {
        $ingredientIds = array_column($inputData['fruits'], 'id');
        $fruits = Fruit::whereIn('id', $ingredientIds)->get();

        $this->inputFruits = collect($inputData['fruits']);

        $fruitsWeight = $this->calculateWeight($fruits);

        $saladTotalWeight = array_sum(array_column($inputData['fruits'], 'weight'));

        return [
            'name' => $inputData['name'],
            'description' => $inputData['description'],
            'weight' => $saladTotalWeight,
            'carbohydrates' => $fruitsWeight->sum('carbohydrates'),
            'protein' => $fruitsWeight->sum('protein'),
            'fat' => $fruitsWeight->sum('fat'),
            'calories' => $fruitsWeight->sum('calories'),
            'sugar' => $fruitsWeight->sum('sugar'),
        ];

    }

    /**
     * calculate Nutrient weight
     *
     * @param $fruits
     * @return mixed
     */
    protected function calculateWeight($fruits): mixed
    {
        return $fruits->map(function ($item) {
            return [
                'carbohydrates' => $item->carbohydrates / 100 * $this->getWeight($item->id),
                'protein' => $item->protein / 100 * $this->getWeight($item->id),
                'fat' => $item->fat / 100 * $this->getWeight($item->id),
                'calories' => $item->calories / 100 * $this->getWeight($item->id),
                'sugar' => $item->sugar / 100 * $this->getWeight($item->id),
            ];
        });
    }

    /**
     * Get weight for all salad's nutrients of fruits accordingly to ingredient weight
     *
     * @param $itemId
     *
     * @return mixed
     */
    protected function getWeight($itemId): mixed
    {
        return $this->inputFruits[$this->inputFruits->search(function ($value) use ($itemId) {
            return $value['id'] === $itemId;
        }
        )]['weight'];
    }

    /** Remove salad and detach fruits
     *
     * @param int $saladId
     * @return bool
     */
    public function removeSalad(int $saladId): bool
    {
        $salad = Salad::find($saladId);

        if ($salad) {
            $salad->fruits()->detach();
            return $salad->delete();
        }

        Redis::del('salad_' . $saladId);
        $this->putSaladsListToRedis();

        return false;
    }

    /**
     * Put salad to Redis
     *
     * @param int $saladId
     * @return void
     */
    protected function putSaladToRedis(int $saladId): void
    {
        $saladWithFruits = json_encode(Salad::where('id', $saladId)->with('fruits')->get());
        Redis::set('salad_' . $saladId, $saladWithFruits);
        $this->putSaladsListToRedis();
    }

    /**
     * Get salad from Redis
     *
     * @param int $saladId
     * @return mixed
     */
    public function getSaladFromRedis(int $saladId): mixed
    {
        $cachedSalad = Redis::get('salad_' . $saladId);
        if (strlen($cachedSalad) > 2) {
            $returnSalad = json_decode($cachedSalad, FALSE);
        } else {
            $saladList = $this->getSaladsListFromRedis();
            if ($returnSalad = $saladList->find($saladId)) {
                Redis::set('salad_' . $saladId, json_encode($returnSalad));
            }
        }
        return $returnSalad ?? false;
    }

    /**
     * Put salads to list in Redis
     *
     * @return void
     */
    public function putSaladsListToRedis(): void
    {
        $saladsListWithRelations = serialize(Salad::with('fruits')->get());
        Redis::set('salads_list', $saladsListWithRelations);
    }

    /**
     * Get Salads list from redis
     *
     * @return mixed
     */
    public function getSaladsListFromRedis(): mixed
    {
        $cachedSaladsList = Redis::get('salads_list');

        if (isset($cachedSaladsList)) {
            $returnList = unserialize($cachedSaladsList);
        }
        return $returnList ?? false;
    }

    /**
     * Search salad(s) by containing fruit(s)
     *
     * @param $fruits
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function searchSaladsFruits($fruits): \Illuminate\Database\Eloquent\Collection
    {
        $fruitIds = $fruits['fruits'];
        return Salad::whereHas('fruits', function($query) use($fruitIds) {
            $query->whereIn('fruits.id', $fruitIds);
        })->get();
    }

    /**
     * Search salad(s) by containing nutrient(s)
     *
     * @param $nutrients
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function searchSaladsNutrients($nutrients): \Illuminate\Database\Eloquent\Collection
    {
        $query = Salad::query();
        foreach ($nutrients as $key => $nutrient) {
            if (count($nutrient) === 1) {
                $query->where($key, $nutrient[0]);
            }
            elseif (count($nutrient) === 2) {
                $query->whereBetween($key, $nutrient);
            }
        }
        return $query->get();
    }

    /**
     * Search salad(s) by nutrient(s) weight
     *
     * @param $weight
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function searchSaladsFruitsWeight($weight): \Illuminate\Database\Eloquent\Collection
    {
        $key = $weight['key'];
        $range = $weight['range'];
        $query = Salad::whereHas('fruits', function ($q) use ($key, $range) {
                $q->where('fruit_id', $key)->whereBetween('fruit_salad.weight', $range);
        })
        ->with('fruits');

        return $query->get();
    }

    /**
     * Sort salads by Nutrients
     *
     * @param
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function sortSaladsByNutrients($data): \Illuminate\Database\Eloquent\Collection
    {
        $query = Salad::with('fruits');

        foreach ($data as $key => $dir) {
            $query->orderBy($key, $dir);
        }

        return $query->get();
    }

    /**
     * Filter salads by nutrients
     *
     * @param
     */
    public function filterSalads($data)
    {

    }

}
