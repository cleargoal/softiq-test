<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Salad extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'carbohydrates', 'protein', 'fat', 'calories', 'sugar', 'weight',];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'carbohydrates' => 'float',
        'protein' => 'float',
        'fat' => 'float',
        'calories' => 'integer',
        'sugar' => 'float',
        'weight' => 'integer',
    ];

    /**
     * Interact with the Salad name
     *
     * @param  float  $value
     * @return Attribute
     */
    public function name(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => ucfirst($value),
            set: fn ($value) => strtolower($value),
        );
    }

    /**
     * Interact with the Salad description
     *
     * @param  string  $value
     * @return Attribute
     */
    public function description(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => ucfirst($value),
            set: fn ($value) => strtolower($value),
        );
    }

    /**
     * Interact with the Salad carbohydrates
     *
     * @param  float  $value
     * @return Attribute
     */
    public function carbohydrates(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * Interact with the Salad protein
     *
     * @param  float  $value
     * @return Attribute
     */
    public function protein(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * Interact with the Salad fat
     *
     * @param  float  $value
     * @return Attribute
     */
    public function fat(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * Interact with the Salad calories
     *
     * @param  integer  $value
     * @return Attribute
     */
    public function calories(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * Interact with the Salad sugar
     *
     * @param  float  $value
     * @return Attribute
     */
    public function sugar(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * Interact with the Salad weight
     *
     * @param  integer  $value
     * @return Attribute
     */
    public function weight(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * Relation to Fruits
     *
     * @return BelongsToMany
     */
    public function fruits(): BelongsToMany
    {
        return $this->belongsToMany(Fruit::class)->as('ingredient')->withPivot('weight');
    }
}
