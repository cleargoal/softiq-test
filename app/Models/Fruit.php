<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Fruit extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'carbohydrates', 'protein', 'fat', 'calories', 'sugar',];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'carbohydrates' => 'float',
        'protein' => 'float',
        'fat' => 'float',
        'calories' => 'integer',
        'sugar' => 'float',
    ];

    /**
     * Interact with the Fruit name
     *
     * @param  string  $value
     * @return Attribute
     */
    public function name(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => ucfirst($value),
            set: fn ($value) => strtolower($value),
        );
    }

    /**
     * Interact with the Fruit carbohydrates
     *
     * @param  float  $value
     * @return Attribute
     */
    public function carbohydrates(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * Interact with the Fruit protein
     *
     * @param  float  $value
     * @return Attribute
     */
    public function protein(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * Interact with the Fruit fat
     *
     * @param  float  $value
     * @return Attribute
     */
    public function fat(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * Interact with the Fruit calories
     *
     * @param  integer  $value
     * @return Attribute
     */
    public function calories(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * Interact with the Fruit sugar
     *
     * @param  float  $value
     * @return Attribute
     */
    public function sugar(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * Relation to Salades
     *
     * @return BelongsToMany
     */
    public function salades(): BelongsToMany
    {
        return $this->belongsToMany(Salad::class)->as('ingredient')->withPivot('weight');
    }
}
